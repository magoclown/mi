// function setTextDiv(msg) {
//   $("div.holaMundo").text(msg);
// }
$(document).ready(() => {
  let setText = msg => {
    $("div.holaMundo").text(msg);
  };
  //   alert("HOLA MUNDO");
  $("body").css("background-color", "gray");
  //   $("div.holaMundo").text("Hola mundo");
  setText("Hola Mundo");
  //   setTimeout(() => {
  //     // $("div.holaMundo").text("Adios");
  //     setText("Adios");
  //   }, 3000);

  //   let loteria = 0;
  //   while (loteria !== 5) {
  //     loteria = Math.floor(Math.random() * 1000000);
  //     // console.log(`Valor ${loteria}`);
  //   }
  //   setText("Adios");

  // let prom = new Promise((resolve, reject) => {
  //   setTimeout(() => {
  //     reject("No se gano la loteria nunca.");
  //   }, 3000);
  //   let loteria = 0;
  //   while (loteria !== 5) {
  //     loteria = Math.floor(Math.random() * 1000000);
  //     console.log(`Valor ${loteria}`);
  //   }
  //   resolve("GANO LA LOTERIA!");
  // });
  // setText("REALIZANDO LOTERIA...");
  // prom.then(msg => {
  //   setText(msg);
  // }).catch(msg => {
  //   setText(msg);
  // });
  // setText("FINALIZO LA LOTERIA");
  $.ajax({
    url: "../mock/data.json"
  }).done(data => {
    console.log(data);
    let comment = data["_comment"];
    console.log(comment);
    let alumnos = data["alumnos"];
    console.log(alumnos);
    alumnos.forEach(element => {
      console.log(element);
    });
    alumnos.forEach((v, k) => {
      console.log(`Key ${k}, Value ${v}`);
    });
  });
  let setFormat = alumno => {
    return `<ol>
<li>Nombre: ${alumno.nombre}</li>
<li>Edad: ${alumno.edad}</li>
<li>Semestre: ${alumno.semestre}</li>
</ol>`;
  };
  $.ajax({
    url: "../mock/data.json"
  }).done(data => {
    let alumnos = data["alumnos"];
    alumnos.forEach(element => {
      // console.log(element);
      $("div.holaMundo").append(setFormat(element));
    });
  });
});
