#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

// Plot a line on Y using a value between 0.0-1.0
float plot(vec2 st, float pct){
  return  smoothstep( pct-0.02, pct, st.y) -
          smoothstep( pct, pct+0.02, st.y);
}

void main() {
	vec2 st = gl_FragCoord.xy/u_resolution;

    float y = st.x;
    vec4 colorBase = vec4(0.2118, 0.102, 0.8471, 1.0);
    vec4 r1 = vec4(0.2667, 0.2667, 0.2667, 1.0);
    vec4 r2 = vec4(0.5412, 0.5412, 0.5412, 1.0);
    vec4 r3 = vec4(0.8471, 0.8471, 0.8471, 1.0);
    vec4 r4 = vec4(1.0, 1.0, 1.0, 1.0);
    vec3 ramp = vec3(y);
    vec4 color = vec4(0);
    // if(ramp.x > .5) {
    //     color = colorBase * .3;
    // }else {
    //     color = colorBase * .8;
    // }
    if(ramp.x < .15) {
        color = colorBase * r1;
    } else if(ramp.x < .4){
        color = colorBase * r2;
    } else if(ramp.x < .9){
        color = colorBase * r3;
    } else if(ramp.x < .98){
        color = colorBase * r4;
    } else {
        color = vec4(1);
    }

	gl_FragColor = color;
}
