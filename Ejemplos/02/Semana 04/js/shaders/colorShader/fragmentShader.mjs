export const fragmentShader = `
precision lowp float;
    float random (vec2 st) {
      return fract(sin(dot(st.xy,
                          vec2(12.9898,78.233)))*
          43758.5453123);
    }
    vec2 truchetPattern(in vec2 _st, in float _index){
      _index = fract(((_index-0.5)*2.0));
      if (_index > 0.75) {
          _st = vec2(1.0) - _st;
      } else if (_index > 0.5) {
          _st = vec2(1.0-_st.x,_st.y);
      } else if (_index > 0.25) {
          _st = 1.0-vec2(1.0-_st.x,_st.y);
      }
      return _st;
  }
    varying lowp vec4 vColor;

    void main(void) {
      vec2 st = gl_FragCoord.xy/vec2(640,480).xy;

      st *= 10.0; // Scale the coordinate system by 10
      vec2 ipos = floor(st);  // get the integer coords
      vec2 fpos = fract(st);  // get the fractional coords

      vec4 color = vec4(vec3(random( ipos )),1);

      vec2 tile = truchetPattern(fpos, random( ipos ));

      float maze = 0.0;

      // Maze
      maze = smoothstep(tile.x-0.3,tile.x,tile.y)-
              smoothstep(tile.x,tile.x+0.3,tile.y);
      vec3 mazeColor = vec3(maze);

      gl_FragColor = color *vec4(mazeColor,1.0) * vColor ;
    }
  `;
  