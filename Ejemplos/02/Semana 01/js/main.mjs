import { ColorModel } from "./models/colorModel.mjs";
function main() {
  // document.getElementById("GL").innerHTML = "test";
  // const arr = document.getElementsByClassName("rojo");
  // arr.forEach(element => {
  //     element.innerHTML = "Test";
  // });
  // $("#GL")
  const canvas = document.querySelector("#GL");
  const cColor = new ColorModel(0, 0, 1, 1);
  try {
    const gl = canvas.getContext("webgl");
    if (gl === null) {
      throw "No se inicializo WebGL.";
    }
    gl.clearColor(cColor.r, cColor.g, cColor.b, cColor.a);
    gl.clear(gl.COLOR_BUFFER_BIT);
  } catch (error) {
    console.error(error);
  }
}

window.onload = main;
