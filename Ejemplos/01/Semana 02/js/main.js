function addClasses(...classses) {
  classses.forEach(element => {
    $("div#holaMundo").addClass(element);
  });
}

$(document).ready(() => {
  let setFormat = alumno => {
    return `<ol>
    <li>Nombre: ${alumno.nombre}</li>
    <li>Edad: ${alumno.edad}</li>
    <li>Semestre: ${alumno.semestre}</li>
    </ol>`;
  };
  $.ajax({
    url: "../mock/data.json"
  }).done(data => {
    // let comment = data["_comment"];
    let comment = data._comment;
    console.info(`Comment ${comment}`);
    let alumnos = data.alumnos;
    console.info(`Alumnos: ${alumnos}`);
    alumnos.forEach(element => {
      // console.info(element);
      $("div#holaMundo").append(setFormat(element));
    });
  });
  // alert("Hola mundo!");
  let prom = new Promise((resolve, reject) => {
    let value = 5;
    setTimeout(() => {
      value += 10;
      resolve(value);
    }, 3000);
    // resolve(value);
  });
  prom
    .then(msg => {
      console.log(msg);
    })
    .catch(err => {
      console.error(err);
    });
  addClasses("rojo", "azul", "bg-negro");
  if (true) {
    let x = 5;
    console.log(x);
    let fun = name => {
      console.log(`fun ${name}`);
    };
    fun("Jose");
  }
  if (false) {
    console.log(x);
    fun();
  }
});
// localhost:5500
