export const vertexShader = `
precision highp float;
attribute vec4 aVertexPosition;
attribute vec3 aVertexNormal;
attribute vec2 aTextureCoord;
uniform mat4 uNormalMatrix;
uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
varying highp vec2 vTextureCoord;
varying highp vec3 vLighting;
uniform float uTimer;
void main(void) {
  float x = abs(sin(uTimer));
  float y = abs(cos(uTimer));
  vec4 pos = aVertexPosition;
  pos.y = pos.y + (x);
  pos.x = pos.x + (y);

  gl_Position = uProjectionMatrix * uModelViewMatrix * pos;
  vTextureCoord = aTextureCoord;
  // Apply lighting effect
  highp vec3 ambientLight = vec3(0.3, 0.3, 0.3);
  highp vec3 directionalLightColor = vec3(1, 1, 1);
  highp vec3 directionalVector = normalize(vec3(0.85, 0.8, 0.75));
  highp vec4 transformedNormal = uNormalMatrix * vec4(aVertexNormal, 1.0);
  highp float directional = max(dot(transformedNormal.xyz, directionalVector), 0.0);
  vLighting = ambientLight + (directionalLightColor * directional);
}
  `;