// Conseguir un div con clase red
// dentro de un div con id black
// document.getElementById("black");
// document.getElementsByClassName("red");
// document.querySelector("#black .red");
import { Color, ColorModel } from "./models/colorModel.mjs";
import * as Colors from "./colorsLibrary.mjs";

/**
 * Nos permite imprimir un log
 * @param message Mensaje a imprimir
 */
function Log(message) {
  console.log(message);
}

/**
 * Main function
 */
function main() {
  Log("texto");
  // var canvas = document.querySelector("#GL");
  // let canvas = document.querySelector("#GL");
  const canvas = document.querySelector("#GL");
  //   const cColor = new ColorModel(0, 0, 1, 1);
  const cColor = Colors.blue;
  try {
    const gl = canvas.getContext("webgl");
    if (gl === null) {
      throw "No se inicializo WebGL.";
    }
    gl.clearColor(cColor.r, cColor.g, cColor.b, cColor.a);
    gl.clear(gl.COLOR_BUFFER_BIT);
  } catch (error) {
    console.error(error);
  }
}
window.onload = main;
